<?php

namespace Ayco\Monolog\Handler;

use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;

/**
 * Send messages to Achat
 *
 * @author Jose García <jgarcia@ayco.net>
 */
class AchatHandler extends AbstractProcessingHandler
{
    private $channel;
    private $username;

    private $webhookUrl;

    public function __construct(
        $webhookUrl,
        $channel = null,
        $username = null,
        $tipo = Logger::DEBUG,
        $bubble = true
    ) {
        $this->channel = $channel;
        $this->username = $username;
        $this->webhookUrl = $webhookUrl;

        parent::__construct($tipo, $bubble);
    }

    protected function write(array $registro)
    {
        $mensaje = sprintf(
            "Origen Log: *%s*\nTipo Log: *%s*\n```%s```",
            $registro['channel'], $registro['level_name'], $registro['message']
        );

        $post = array_filter([
            'username' => $this->username,
            'icon_emoji' => 'https://raw.githubusercontent.com/alphanodes/redmine_messenger/master/assets/images/icon.png',
            'channel' => $this->channel,
            'text' => $mensaje,
        ]);

        $ch = curl_init();
        $options = array(
            CURLOPT_URL => $this->webhookUrl,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array('Content-type: application/json'),
            CURLOPT_POSTFIELDS => json_encode($post),
            CURLOPT_SSL_VERIFYPEER => false
        );
        if (defined('CURLOPT_SAFE_UPLOAD')) {
            $options[CURLOPT_SAFE_UPLOAD] = true;
        }
        curl_setopt_array($ch, $options);
        curl_exec($ch);
    }
}
